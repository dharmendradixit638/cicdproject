import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Header from './components/Header_component/Header';
import Home from './components/Home_component/Home';
import Aboutus from './components/Aboutus_component/Aboutus';
import Contactus from './components/Contactus_component/Contactus';
import Footer from './components/Footer_component/Footer';
import Notfound from './components/Notfound_component/Notfound';

function App() {
  return (
    <Router>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/aboutus" element={<Aboutus />} />
        <Route path="/contactus" element={<Contactus />} />
        <Route path="*" element={<Notfound />} />
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;